package api

type Note struct {
	ID     int         `jsonapi:"primary,notes"`
	Nr     int         `jsonapi:"attr,nr"`
	Name   string      `jsonapi:"attr,name"`
	Author *string     `jsonapi:"attr,author,omitempty"`
	Active bool        `jsonapi:"attr,active"`
	Files  []*NoteFile `jsonapi:"relation,files"`
}

type NoteFile struct {
	ID       int    `jsonapi:"primary,files"`
	Name     string `jsonapi:"attr,name"`
	FileType string `jsonapi:"attr,filetype"` // Follows MIME type
	NoteID   int    `jsonapi:"attr,note_id"`  // Matches Note.ID
}

package server

import (
	"context"
	"net/http"
)

func withServerContextMiddleware(s *Server, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		newRequest := r.WithContext(context.WithValue(r.Context(), SERVER_CONTEXT_KEY, s))
		next.ServeHTTP(w, newRequest)
	})
}

package server

import (
	"fmt"
	"io/fs"
	"net/http"

	"gitlab.com/mstarongitlab/weblogger"

	"gitlab.com/mstarongitlab/mv-notes/storage"
)

type ServerContext string
type Server struct {
	Storage       *storage.Storage
	FrontendFiles fs.FS
	router        http.Handler
}

const SERVER_CONTEXT_KEY = ServerContext("net.evilthings.server-context")

func NewServer(frontendFiles fs.FS, indexFile string, s *storage.Storage) *Server {
	// Routes for /api/v1 that require authentication
	adminRouterV1 := http.NewServeMux()
	adminRouterV1.HandleFunc("PATCH /notes", handleNotesPatch)
	adminRouterV1.HandleFunc("POST /notes", handleNotesPost)
	adminRouterV1.HandleFunc("/test-auth", func(w http.ResponseWriter, r *http.Request) {
		// No need to actually check here if the authentication is ok.
		// That part is completed by the adminMiddleware this route is behind
		http.Error(w, "authentication ok", http.StatusOK)
	})

	// All routes for /api/v1
	routerV1 := http.NewServeMux()
	routerV1.Handle(
		"/",
		// FIX: Use proper auth function here
		adminMiddleware(adminRouterV1, func(s string) bool { return s == _TOKEN }),
	)
	routerV1.HandleFunc("GET /notes", handleNotesGet)
	routerV1.HandleFunc("POST /login", handleLoginRoute)

	// All api versions
	apiRouter := http.NewServeMux()
	apiRouter.Handle("/v1/", http.StripPrefix("/v1", routerV1))

	// Root (/) router
	fullRouter := http.NewServeMux()
	fullRouter.Handle("/api/", http.StripPrefix("/api", apiRouter))
	fullRouter.HandleFunc("GET /", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, indexFile)
	})

	server := &Server{
		FrontendFiles: frontendFiles,
		Storage:       s,
		router:        fullRouter,
	}

	server.router = weblogger.LoggingMiddleware(
		withServerContextMiddleware(server, server.router),
		weblogger.LOGGING_SEND_REQUESTS_DEBUG,
	)

	return server
}

func (s *Server) Run(addr string) error {
	return http.ListenAndServe(addr, s.router)
}

func getServerFromRequest(r *http.Request) *Server {
	server, ok := r.Context().Value(SERVER_CONTEXT_KEY).(*Server)
	if ok {
		return server
	} else {
		return nil
	}
}

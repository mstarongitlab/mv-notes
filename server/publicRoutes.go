package server

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type authRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type authResponse struct {
	Token string `json:"token"`
}

// FIX: No hardcoded credentials and token please
const _USERNAME = "noteAdmin"
const _PASSWORD = "some hardcoded password"
const _TOKEN = "insert token here"

// Handler for /api/v1/login
func handleLoginRoute(w http.ResponseWriter, r *http.Request) {
	server := getServerFromRequest(r)
	if server == nil {
		http.Error(w, "failed to get server from context", http.StatusInternalServerError)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "missing body", http.StatusBadRequest)
		return
	}
	request := authRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		http.Error(w, fmt.Sprintf("bad body. Error: %s", err.Error()), http.StatusBadRequest)
		return
	}
	// FIX: Please no hardcoded credentials
	if request.Username != _USERNAME || request.Password != _PASSWORD {
		http.Error(w, "bad credentials", http.StatusUnauthorized)
		return
	}
	rawJson, err := json.Marshal(&authResponse{
		Token: _TOKEN, // FIX: Add proper token generation
	})
	if err != nil {
		http.Error(w, "json marshaling failed", http.StatusInternalServerError)
		return
	}
	r.Header.Set("Content-Type", "application/json")
	_, _ = w.Write(rawJson)
}

// Handler func for GET requests to /api/v1/notes
func handleNotesGet(w http.ResponseWriter, r *http.Request) {
	server := getServerFromRequest(r)
	if server == nil {
		http.Error(w, "failed to get server from context", http.StatusInternalServerError)
		return
	}
}

package server

import "net/http"

// Handler func for PATCH requests to /api/v1/notes
func handleNotesPatch(w http.ResponseWriter, r *http.Request) {
	server := getServerFromRequest(r)
	if server == nil {
		http.Error(w, "failed to get server from context", http.StatusInternalServerError)
		return
	}
}

// Handler func for POST requests to /api/v1/notes
func handleNotesPost(w http.ResponseWriter, r *http.Request) {
	server := getServerFromRequest(r)
	if server == nil {
		http.Error(w, "failed to get server from context", http.StatusInternalServerError)
		return
	}
}

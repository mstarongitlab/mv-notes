package server

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

func adminMiddleware(prevHandler http.Handler, tokenCheck func(string) bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if token := r.Header.Get("authorisation"); token != "" {
			logrus.WithFields(logrus.Fields{
				"token": token,
				"path":  r.URL,
			}).Debugln("Attempting authentication")
			if !tokenCheck(token) {
				http.Error(w, "unauthorised", http.StatusUnauthorized)
				return
			}
			prevHandler.ServeHTTP(w, r)
		} else {
			http.Error(w, "unauthorised", http.StatusUnauthorized)
			return
		}
	})
}

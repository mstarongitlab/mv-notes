module gitlab.com/mstarongitlab/mv-notes

go 1.22.1

require (
	github.com/julienschmidt/httprouter v1.3.0
	github.com/sirupsen/logrus v1.9.3
	// UNUSED gitlab.com/mstarongitlab/weblogger v0.0.0-20240123135616-d64461e3b20d
	gorm.io/driver/sqlite v1.5.4
	gorm.io/gorm v1.25.5
)

require (
	github.com/google/uuid v1.6.0
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	gitlab.com/mstarongitlab/goutils v0.0.0-20240221131250-70f6d1947636
	gitlab.com/mstarongitlab/weblogger v0.0.0-20240228083417-161d4aea06ec
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)

require github.com/google/jsonapi v1.0.0 // indirect

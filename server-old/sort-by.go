package serverOld

import (
	"html/template"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/mstarongitlab/goutils/sliceutils"
	"gitlab.com/mstarongitlab/mv-notes/storage"
)

func CreateSortByHandler(server *Server) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		tmpl := template.Must(template.ParseFS(server.TemplateFs, "index.html"))
		sortingMethodString := r.PostFormValue("sorting-method")
		sortingMethod := storage.SORT_BY_NAME
		if sortingMethodString == "sort-by-number" {
			sortingMethod = storage.SORT_BY_NR
		}
		entries, err := server.Storage.GetNoteEntries(sortingMethod, r.PostFormValue("active-only") != "")
		logrus.WithFields(logrus.Fields{
			"sorting-method": sortingMethodString,
			"active-only":    r.PostFormValue("active-only") != "",
			"results":        entries,
		}).Debugln("Got sorted notes")
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"sorting-method": sortingMethod,
				"active-only":    r.PostFormValue("active-only") != "",
			}).Errorln("Failed to get notes for table")
			http.Error(w, "failed to get notes", http.StatusInternalServerError)
			return
		}
		loggedIn, loggedID := checkLogin(server, r)
		err = tmpl.ExecuteTemplate(w, "entries", struct {
			Notes []noteRenderData
		}{
			Notes: sliceutils.Map(entries, func(note storage.NoteEntry) noteRenderData {
				return noteRenderData{
					Name:      note.Name,
					CurrentNr: note.CurrentNr,
					Author:    note.Author,
					Active:    note.Active,
					LoginID:   loggedID,
					LoggedIn:  loggedIn,
				}
			}),
		})
		if err != nil {
			logrus.WithError(err).Errorln("Template entries failed for index")
		}
	}
}

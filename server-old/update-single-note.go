package serverOld

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/mstarongitlab/goutils/sliceutils"
	"gitlab.com/mstarongitlab/mv-notes/storage"
)

func CreateUpdateSingleNoteHandler(server *Server) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		tmpl, err := template.ParseFS(server.TemplateFs, "index.html")
		if err != nil {
			logrus.WithError(err).Errorln("Failed to build template for index")
			http.Error(w, fmt.Errorf("failed to build template: %w", err).Error(), http.StatusInternalServerError)
			return
		}
		loggedIn, loggedId := checkLogin(server, r)
		noteNewNr, err := strconv.Atoi(r.PostFormValue("new-nr"))
		if err != nil {
			logrus.WithField("form", r.PostForm).Infof("Failed to convert new nr %s to int\n", r.PostFormValue("number"))
			http.Error(w, "number must be an int32", http.StatusBadRequest)
			return
		}
		noteName := r.PostFormValue("name")
		noteNewActive := r.PostFormValue("active") == "on"
		noteAuthorRaw := r.PostFormValue("author")
		noteOldNr, err := strconv.Atoi(r.PostFormValue("oldNr"))
		if err != nil {
			logrus.WithField("form", r.PostForm).Infof("Failed to convert old nr %s to int\n", r.PostFormValue("oldNr"))
			http.Error(w, "oldNr must be an int32", http.StatusBadRequest)
			return
		}
		var noteAuthor *string = nil
		if noteAuthorRaw != "" {
			noteAuthor = &noteAuthorRaw
		}

		files, filesErr := server.Storage.GetFilesFor(noteName)

		if !loggedIn {
			logrus.WithField("post-form", r.PostForm.Encode()).Warningln("Got attempted single note update with invalid login, ignoring")
			err = tmpl.ExecuteTemplate(w, "note", noteRenderData{
				Name:      noteName,
				CurrentNr: noteOldNr,
				Author:    noteAuthor,
				Active:    noteNewActive,
				LoggedIn:  false,
				HasFiles:  filesErr == nil && len(files) > 0,
				Files: sliceutils.Map(files, func(x string) fileData {
					return fileData{
						File:     x,
						NoteName: noteName,
					}
				}),
			})
			if err != nil {
				logrus.WithError(err).Error("Failed to execute note template when updating single note after failed authentication")
			}
			return
		}

		newNote := storage.NoteEntry{
			Name:      noteName,
			Active:    noteNewActive,
			CurrentNr: noteNewNr,
			Author:    noteAuthor,
			// WasNr is for updater to figure out
		}
		err = server.Storage.UpdateEntry(&newNote)
		if err != nil {
			logrus.WithError(err).WithField("post-form", r.PostForm.Encode()).Warningln("Failed to update note, keeping old values")
			err = tmpl.ExecuteTemplate(w, "note", &noteRenderData{
				Name:      newNote.Name,
				CurrentNr: noteOldNr,
				Author:    newNote.Author,
				Active:    newNote.Active,
				LoginID:   loggedId,
				LoggedIn:  loggedIn,
				HasFiles:  filesErr == nil && len(files) > 0,
				Files: sliceutils.Map(files, func(x string) fileData {
					return fileData{
						File:     x,
						NoteName: noteName,
					}
				}),
			})
			if err != nil {
				logrus.WithError(err).Error("Failed to execute note template when updating single note after failed update")
			}
			return
		}

		err = tmpl.ExecuteTemplate(w, "note", &noteRenderData{
			Name:      newNote.Name,
			CurrentNr: newNote.CurrentNr,
			Author:    newNote.Author,
			Active:    newNote.Active,
			LoginID:   loggedId,
			LoggedIn:  loggedIn,
		})
		if err != nil {
			logrus.WithError(err).Error("Failed to execute note template when updating single note")
		}
	}
}

package serverOld

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/mstarongitlab/goutils/sliceutils"
	"gitlab.com/mstarongitlab/mv-notes/storage"
)

// For now, hardcode these values
// TODO: Store them locally, use hash for password and allow updating
const (
	ADMIN_NAME = "admin"
	ADMIN_PW   = "admin"
)

type IndexData struct {
	Notes         []noteRenderData
	SortingMethod storage.SortMethod
	ActiveOnly    bool
	LoggedIn      bool
	LoginID       string
}

type noteRenderData struct {
	Name      string
	CurrentNr int
	Author    *string
	Active    bool
	LoginID   string
	LoggedIn  bool
	HasFiles  bool
	Files     []fileData // Only the name. Path of the file is SCAN_DIR/<Name>/<Filename>
}

type fileData struct {
	File     string
	NoteName string
}

func CreateIndexHandler(server *Server) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		templ, err := template.ParseFS(server.TemplateFs, "index.html")
		if err != nil {
			logrus.WithError(err).Errorln("Failed to build template for index")
			http.Error(w, fmt.Errorf("failed to build template: %w", err).Error(), http.StatusInternalServerError)
			return
		}

		entries, err := server.Storage.GetNoteEntries(storage.SORT_BY_NR, false)
		if err != nil {
			http.Error(w, fmt.Errorf("failed to get note entries: %w", err).Error(), http.StatusInternalServerError)
			return
		}

		loginOk, loginID := checkLogin(server, r)
		logrus.WithFields(logrus.Fields{
			"LoginOK":   loginOk,
			"LoginOD":   loginID,
			"AllLogins": server.ActiveLogins,
		}).Debugln("Login check results")

		data := IndexData{
			Notes: sliceutils.Map(entries, func(note storage.NoteEntry) noteRenderData {
				files, err := server.Storage.GetFilesFor(note.Name)
				return noteRenderData{
					Name:      note.Name,
					CurrentNr: note.CurrentNr,
					Author:    note.Author,
					Active:    note.Active,
					LoginID:   loginID,
					LoggedIn:  loginOk,
					HasFiles:  err == nil && len(files) > 0,
					Files: sliceutils.Map(files, func(x string) fileData {
						return fileData{
							File:     x,
							NoteName: note.Name,
						}
					}),
				}
			}),
			SortingMethod: storage.SORT_BY_NR,
			ActiveOnly:    false,
			LoggedIn:      loginOk,
			LoginID:       loginID,
		}
		err = templ.Execute(w, &data)
		if err != nil {
			logrus.WithError(err).Errorln("Failed to execute index template")
			return
		}
	}
}

// Checks whether a request is logged in
// Returns true and the login id if properly authenticated, false otherwise
func checkLogin(server *Server, r *http.Request) (bool, string) {
	id := r.PostFormValue("login-id")
	logrus.WithField("LoginID", id).Debugln("Attempted authentication")
	if id != "" {
		val, ok := server.ActiveLogins[id]
		// Case login id not found in server, log them out
		if !ok {
			return false, ""
		}
		// Case login older than 1 day
		if time.Since(val).Hours() > 24 {
			delete(server.ActiveLogins, id)
			return false, ""
		}
		return true, id
	}
	username := r.PostFormValue("username")
	if username != ADMIN_NAME {
		return false, ""
	}
	pw := r.PostFormValue("password")
	if pw != ADMIN_PW {
		return false, ""
	}
	x := uuid.NewString()
	server.ActiveLogins[x] = time.Now()
	return true, x
}

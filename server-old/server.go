package serverOld

import (
	"io/fs"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/mstarongitlab/mv-notes/storage"
	"gitlab.com/mstarongitlab/weblogger"
)

type Server struct {
	router     *httprouter.Router
	Handler    http.Handler
	Storage    *storage.Storage
	TemplateFs fs.FS
	// Key is the login id, value is the time the login was first created
	ActiveLogins map[string]time.Time
}

func NewServer(s *storage.Storage, templates, static fs.FS, noteFiles fs.FS) *Server {
	router := httprouter.New()
	server := &Server{
		router:       router,
		Storage:      s,
		TemplateFs:   templates,
		Handler:      weblogger.LoggingMiddleware(router, weblogger.LOGGING_SEND_REQUESTS_DEBUG),
		ActiveLogins: map[string]time.Time{},
	}
	router.GET("/", CreateIndexHandler(server))
	router.POST("/", CreateIndexHandler(server))
	router.POST("/add-note", AddNoteHandler(server))
	router.POST("/sort-by", CreateSortByHandler(server))
	router.POST("/update-single-note", CreateUpdateSingleNoteHandler(server))
	router.ServeFiles("/static/*filepath", http.FS(static))
	router.ServeFiles("/note-files/*filepath", http.FS(noteFiles))
	return server
}

func (server *Server) Run(addr string) error {
	logrus.WithField("address", addr).Infoln("Starting server")
	return http.ListenAndServe(addr, server.Handler)
}

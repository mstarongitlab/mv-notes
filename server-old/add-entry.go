package serverOld

import (
	"errors"
	"html/template"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/mstarongitlab/mv-notes/storage"
)

func AddNoteHandler(server *Server) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		loggedIn, loggedID := checkLogin(server, r)
		if !loggedIn {
			return
		}

		entry_number, err := strconv.Atoi(r.PostFormValue("nummer"))
		entry_name := r.PostFormValue("name")
		entry_author := r.PostFormValue("author")
		entry_active := r.PostFormValue("active") == "on"

		if err != nil {
			http.Error(w, "number must be a number", http.StatusBadRequest)
			logrus.WithError(err).WithField("given_number", r.PostFormValue("nummer")).Errorln("number for new note wasn't a number")
			return
		}

		var parsed_author *string = nil
		if entry_author != "" {
			parsed_author = &entry_author
		}

		new_note := storage.NoteEntry{
			Name:      entry_name,
			CurrentNr: entry_number,
			Author:    parsed_author,
			WasNr:     entry_number,
			Active:    entry_active,
		}

		err = server.Storage.AddEntry(&new_note)

		if err != nil && !errors.Is(err, storage.ErrHadToIncreaseNumber) {
			return
		}
		if errors.Is(err, storage.ErrHadToIncreaseNumber) {
			logrus.WithError(err).WithFields(logrus.Fields{
				"old-number": entry_number,
				"new-number": new_note.CurrentNr,
				"title":      entry_name,
			}).Warnln("Had to increase number")
		}

		tmpl, err := template.ParseFS(server.TemplateFs, "index.html")
		if err != nil {
			logrus.WithError(err).Errorln("Failed to parse note index template")
			return
		}

		// TODO: Add creating a folder for new notes

		fullData := noteRenderData{
			new_note.Name,
			new_note.CurrentNr,
			new_note.Author,
			new_note.Active,
			loggedID,
			loggedIn,
			false,
			[]fileData{}, // Empty file data since new file obviously doesn't have any files stored yet
		}
		err = tmpl.ExecuteTemplate(w, "note", &fullData)
		if err != nil {
			logrus.WithError(err).Errorln("Failed to run note template for index")
		}
	}
}

package storage

import (
	"errors"
	"fmt"
	"slices"
	"strings"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type SortMethod int

const (
	SORT_BY_NR = SortMethod(iota)
	SORT_BY_NAME
)

type NoteEntry struct {
	gorm.Model
	Name      string
	CurrentNr int
	Author    *string
	WasNr     int
	Active    bool
}

var ErrHadToIncreaseNumber = errors.New("had to increase number of new entry to fit")
var ErrUpdateToExistingNr = errors.New("can't update number of entry as another entry already uses that number")

func (storage *Storage) GetNoteEntries(sortBy SortMethod, activeOnly bool) ([]NoteEntry, error) {
	notes := []NoteEntry{}
	if activeOnly {
		storage.db.Table("note_entries").Where("active = ?", true).Scan(&notes)
	} else {
		storage.db.Table("note_entries").Scan(&notes)
	}
	switch sortBy {
	case SORT_BY_NAME:
		slices.SortFunc(notes, func(a, b NoteEntry) int { return strings.Compare(a.Name, b.Name) })
	case SORT_BY_NR:
		slices.SortFunc(notes, func(a, b NoteEntry) int { return a.CurrentNr - b.CurrentNr })
	}

	return notes, nil
}

func (storage *Storage) AddEntry(entry *NoteEntry) error {
	logrus.WithField("note", entry).Debugln("Trying to add note")
	placeholder_note := NoteEntry{}
	// Check if name exists first
	result := storage.db.Table("note_entries").First(&placeholder_note, "name = ?", entry.Name)
	if result.RowsAffected > 0 {
		logrus.WithError(result.Error).WithField("entry", entry).Warningln("Note with that name already exists")
		return errors.New("note with same name already exists")
	}

	// Check if same number already exists. If yes, increase target number of new entry until the new number is free
	// Loop explanation:
	// - Get first entry with matching number from db
	// - RowsAffected > 0 -> Entry found
	// - Increase number of new entry by one
	// Note: Integer overflow *could* in theory happen, but who will store 2**31-1 note sheets?
	// Also, if number is increased, note that in the return error
	hadToIncreaseNumber := false
	logrus.Debugln("AddEntry: Scanning for existing numbers")
	result = storage.db.Table("note_entries").First(&placeholder_note, "current_nr = ?", entry.CurrentNr)
	for result.RowsAffected > 0 {
		hadToIncreaseNumber = true
		entry.CurrentNr += 1
		result = storage.db.Table("note_entries").First(&placeholder_note, "current_nr = ?", entry.CurrentNr)
	}

	result = storage.db.Table("note_entries").Create(entry)
	if result.Error != nil {
		return result.Error
	}
	if hadToIncreaseNumber {
		return ErrHadToIncreaseNumber
	}
	return nil
}

func (storage *Storage) UpdateEntry(new *NoteEntry) error {
	logrus.WithField("new-note", new).Debugln("Attempting to update note")
	storedNote := NoteEntry{}
	res := storage.db.Table("note_entries").First(&storedNote, "name = ?", new.Name)
	if res.Error != nil {
		return fmt.Errorf("failed to get old note for %s: %w", new.Name, res.Error)
	}
	tmpNote := NoteEntry{}
	res = storage.db.Table("note_entries").First(&tmpNote, "current_nr = ?", new.CurrentNr)
	if res.RowsAffected > 0 && tmpNote.Name != new.Name {
		return ErrUpdateToExistingNr
	}
	storedNote.WasNr = storedNote.CurrentNr
	storedNote.CurrentNr = new.CurrentNr
	storedNote.Active = new.Active
	storage.db.Save(&storedNote)
	return nil
}

package storage

import (
	"errors"
	"fmt"
	"io/fs"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/mstarongitlab/goutils/sliceutils"
)

var ErrFileAlreadyExists = errors.New("file already exists")

func (storage *Storage) GetFilesFor(note string) ([]string, error) {
	entries, err := fs.ReadDir(storage.scansFS, note)
	if err != nil {
		// Always return an empty array
		return []string{}, fmt.Errorf("error trying to read files for note-set %s: %w", note, err)
	}
	return sliceutils.Map(sliceutils.Filter(entries, func(in fs.DirEntry) bool {
		return !in.IsDir()
	}), func(in fs.DirEntry) string {
		return in.Name()
	}), nil
}

func (storage *Storage) AddFile(note, name string, content []byte) error {
	logrus.WithFields(logrus.Fields{
		"note": note,
		"name": name,
	}).Debugln("Adding file")
	// First check if file already exists
	f, err := storage.scansFS.Open(note + "/" + name)
	f.Close()
	if err != nil {
		return fmt.Errorf("%s/%s already exists: %w", note, name, err)
	}

	err = os.WriteFile(storage.scanDirName+"/"+note+"/"+name, content, 0644)
	if err != nil {
		return fmt.Errorf("failed to create file %s/%s: %w", note, name, err)
	}
	return nil
}

func (storage *Storage) DeleteFile(note, name string) error {
	return os.Remove(storage.scanDirName + "/" + note + "/" + name)
}

package storage

import (
	"fmt"
	"io/fs"

	"github.com/sirupsen/logrus"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Storage struct {
	db          *gorm.DB
	scansFS     fs.FS
	scanDirName string
}

func NewStorage(sqliteLocation string, scanFS fs.FS, scanDirName string) (*Storage, error) {

	logrus.WithField("db-path", sqliteLocation).Infoln("Opening db")
	db, err := gorm.Open(sqlite.Open(sqliteLocation), &gorm.Config{
		Logger: logger.New(
			logrus.StandardLogger(),
			logger.Config{
				LogLevel:                  logger.Error,
				IgnoreRecordNotFoundError: true,
				Colorful:                  false,
				ParameterizedQueries:      true,
			},
		),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to open db at %s: %w", sqliteLocation, err)
	}

	logrus.Infoln("Applying migrations if necessary")
	err = db.AutoMigrate(&NoteEntry{})
	if err != nil {
		return nil, fmt.Errorf("failed to apply migrations: %w", err)
	}

	return &Storage{
		db:          db,
		scansFS:     scanFS,
		scanDirName: scanDirName,
	}, nil
}

import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-src/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | note-table', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<NoteTable />`);

    assert.dom().hasText('');

    // Template block usage:
    await render(hbs`
      <NoteTable>
        template block text
      </NoteTable>
    `);

    assert.dom().hasText('template block text');
  });
});

import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-src/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import Note from 'frontend-src/models/note';

module('Integration | Component | note', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    const note = new Note();
    note.name = 'foo';
    note.nr = 20;
    note.author = undefined;

    await render(hbs`<Note @note="note" />`);

    assert.dom('.note-nr').hasText('20');
    assert.dom('.note-name').hasText('foo');
    assert.dom('.note-author').hasText('---');
  });
});

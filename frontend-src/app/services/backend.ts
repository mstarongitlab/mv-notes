import Service from '@ember/service';
import type Note from 'frontend-src/models/note';
import { tracked } from '@glimmer/tracking';
import { sort } from '@ember/object/computed';
import { compare } from '@ember/utils';

const SORT_BY_NAME = 'sort-by-name';
const SORT_BY_NUMBER = 'sort-by-number';

export default class BackendService extends Service {
  @tracked notes: Note[] = [];

  sortNotes(sortBy: string) {
    switch (sortBy) {
      case SORT_BY_NAME:
        this.notes.sort((a: Note, b: Note) => {
          return compare(a.name, b.name);
        });
        break;
      case SORT_BY_NUMBER:
        this.notes.sort((a: Note, b: Note) => {
          return compare(a.nr, b.nr);
        });
        break;
      default:
        break;
    }
  }
}

// Don't remove this declaration: this is what enables TypeScript to resolve
// this service using `Owner.lookup('service:backend')`, as well
// as to check when you pass the service name as an argument to the decorator,
// like `@service('backend') declare altName: BackendService;`.
declare module '@ember/service' {
  interface Registry {
    backend: BackendService;
  }
}

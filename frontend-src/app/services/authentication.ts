import Service from '@ember/service';

export default class AuthenticationService extends Service {
  _sessionToken: string | null = null;

  async authenticate(username: string, password: string): Promise<boolean> {
    if (this.authenticated) {
      return true;
    }
    const currentDomain = window.location.host;
    try {
      const response = await fetch(currentDomain + '/api/v1/login', {
        method: 'POST',
        body: JSON.stringify({
          username: username,
          password: password,
        }),
      });
      const jsonAnswer = await response.json();
    } catch (e) {}
    return false;
  }

  public get authenticated(): boolean {
    return this._sessionToken != null;
  }
}

// Don't remove this declaration: this is what enables TypeScript to resolve
// this service using `Owner.lookup('service:authentication')`, as well
// as to check when you pass the service name as an argument to the decorator,
// like `@service('authentication') declare altName: AuthenticationService;`.
declare module '@ember/service' {
  interface Registry {
    authentication: AuthenticationService;
  }
}

import { action } from '@ember/object';
import Component from '@glimmer/component';
import type Note from 'frontend-src/models/note';

export interface NoteSignature {
  // The arguments accepted by the component
  Args: {
    note: Note;
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class NoteComponent extends Component<NoteSignature> {
  @action printNote() {
    console.log(`${this.args.note}`);
  }
}

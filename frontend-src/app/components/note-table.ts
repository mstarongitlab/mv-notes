import Component from '@glimmer/component';
import Note from 'frontend-src/models/note';

export interface NoteTableSignature {
  // The arguments accepted by the component
  Args: {
    notes: Note[];
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class NoteTableComponent extends Component<NoteTableSignature> {}

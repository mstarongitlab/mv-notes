import Model, { attr, belongsTo } from '@ember-data/model';
import Note from './note';

export default class NoteFile extends Model {
  @belongsTo('note_id') note: Note = new Note();
  @attr name: string = 'Placeholder';
  @attr file_type: string = 'text/html';
  @attr note_id: number = 0;
}

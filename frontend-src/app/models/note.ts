import Model, { attr, hasMany } from '@ember-data/model';
import type NoteFile from './note_file';

export default class Note extends Model {
  @attr nr: number = 0;
  @attr name: string = 'Placeholder';
  @attr author: string | undefined = undefined;
  @attr active: boolean = false;
  @hasMany('files') files: NoteFile[] = [];
}

# syntax=docker/dockerfile:1
FROM nodejs as build-ember-stage

FROM golang:1.22.1 as build-go-stage
WORKDIR /app

# Setup module dependencies
COPY go.mod ./
COPY go.sum ./
RUN go mod download

# Copy the rest of the files
COPY *.go ./
COPY ./templates/ ./templates/
COPY ./storage/ ./storage/
COPY ./static/ ./static/
COPY ./server/ ./server/
COPY ./fswrapper/ ./fswrapper/

# Then build the binary
RUN CGO_ENABLED=0 GOOS=linux go build -o /mv-notes

# Pull in a minimal image for exported container
FROM gcr.io/distroless/base-debian11 AS build-release-stage

WORKDIR /

# Copy built binary from build container to exported container
COPY --from=build-stage /mv-notes /mv-notes

# Tell Docker to expose port 8080 as that is the default port the server runs on
EXPOSE 8080

# Run as non-root
USER nonroot:nonroot

# And tell exported container where to start
ENTRYPOINT [ "/mv-notes" ]

package main

import (
	"embed"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/mstarongitlab/mv-notes/fswrapper"
	"gitlab.com/mstarongitlab/mv-notes/server"
	"gitlab.com/mstarongitlab/mv-notes/storage"
)

const DB_FILE_DEFAULT = "./db.sqlite"
const LOG_FILE_DEFAULT = "./log.txt"
const PORT_DEFAULT = "8080"
const SCANS_PATH_DEFAULT = "scans/"

//go:embed frontend_build/assets
var FRONTEND_BUILD_FS embed.FS

//go:embed frontend_build/index.html
var INDEX_FILE string

func main() {
	//logrus.SetLevel(logrus.DebugLevel)
	// logrus.SetFormatter(&logrus.JSONFormatter{})

	// Print help menu. Doesn't run the server afterwards
	if len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "--help") {
		printHelpText()
		return
	}

	// Get path to logfile
	logFile := os.Getenv("LOG_FILE")
	if logFile == "" {
		logFile = LOG_FILE_DEFAULT
	}

	// Get port, default to 8080
	port := os.Getenv("PORT")
	if port == "" {
		port = PORT_DEFAULT
	}
	// And check if it's ok or if sudo is required
	tmp := checkPort(port)
	switch tmp {
	// All good
	case 0:
		break
	// Case port number is in range of 1-1000 (also small check for Windows in here :3)
	case 1:
		switch os.Getuid() {
		// Running on Windows, currently not supported yet
		case -1:
			fmt.Println("Running on Windows is not yet supported! Quitting")
			return
		// uid of root is 0
		case 0:
			break
		default:
			fmt.Printf("A portnumber of %s requires root permissions to run! Quitting\n", port)
			return
		}
	// Case not a valid number
	case 2:
		fmt.Printf("%s is not a valid number! Quitting\n", port)
		return
	// Should never happen. Bad result from checkPort
	default:
		fmt.Printf(
			"Got invalid result %d from checkPort. Please file an issue with your environment attached\n",
			tmp,
		)
		return
	}

	// Get dir where to store files at
	scans_path := os.Getenv("SCAN_DIR")
	if scans_path == "" {
		scans_path = SCANS_PATH_DEFAULT
	} else if !strings.HasSuffix(scans_path, "/") {
		scans_path = scans_path + "/"
	}

	// Get path to sqlite file
	dbFile := os.Getenv("DB_FILE")
	if dbFile == "" {
		dbFile = DB_FILE_DEFAULT
	}

	logrusUsesOnlyStdout := false
	// Configure logrus
	file, err := os.OpenFile(logFile, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		logrus.SetOutput(io.MultiWriter(os.Stdout))
		logrus.WithError(err).Warningln("Failed to open log file, logging to stdout only")
		logrusUsesOnlyStdout = true
	} else {
		logrus.SetOutput(io.MultiWriter(os.Stdout, file))
	}

	logrus.WithFields(logrus.Fields{
		"LOG_FILE":            logFile,
		"PORT":                port,
		"SCAN_DIR":            scans_path,
		"DB_FILE":             dbFile,
		"logrus-only-std-out": logrusUsesOnlyStdout,
	}).Infoln("Loaded environment")

	// First make sure that the db file exists or try to create it
	db_file, err := os.Open(dbFile)
	if errors.Is(err, os.ErrNotExist) {
		logrus.WithField("filename", dbFile).Warnln("DB file doesn't exist. Trying to create")
		db_file, err = os.Create(dbFile)
		if err != nil {
			logrus.WithError(err).
				WithField("filename", dbFile).
				Fatal("Couldn't find or create DB file. Quitting")
			return
		}
	} else if err != nil {
		logrus.WithError(err).WithField("filename", dbFile).Errorln("Failed to open DB file. Quitting")
		return
	}
	db_file.Close()
	// And then immediatelly close it to prevent problems with gorm

	// scanFS := fswrapper.NewFSWrapper(os.DirFS(scans_path), scans_path)
	scanFS := os.DirFS(scans_path)

	// Build storage
	storage, err := storage.NewStorage(DB_FILE_DEFAULT, scanFS, scans_path)
	if err != nil {
		logrus.WithError(err).Fatal("Couldn't create storage system. Quitting")
	}

	server := server.NewServer(
		fswrapper.NewFSWrapper(FRONTEND_BUILD_FS, "frontend_build"),
		INDEX_FILE,
		storage,
	)

	// And send it
	err = server.Run(":" + port)
	if err != nil {
		panic(err)
	}
}

// Check if a given port is ok or requires sudo
// 0 == ok
// 1 == requires sudo
// 2 == Invalid value
func checkPort(portString string) int {
	port, err := strconv.Atoi(portString)
	if err != nil || port < 1 {
		return 2
	}

	if port < 1000 {
		return 1
	}
	return 0
}

func printHelpText() {
	fmt.Println("A webserver for managing sheets for orchestras")
	fmt.Println("")
	fmt.Println("Environment arguments:")
	fmt.Println("- LOG_FILE: Where to store logs to, in addition to stdout")
	fmt.Println("  - Default: \"./log.txt\"")
	fmt.Println("- PORT: The port the server should run on")
	fmt.Println("  - Default: \"8080\"")
	fmt.Println("  - Note: A number in the range of 1 - 1000 requires root permission")
	fmt.Println(" - SCAN_DIR: Where to store uploaded files")
	fmt.Println("   - Default: \"scans/\"")
	fmt.Println("- DB_FILE: Where to store sheet info")
	fmt.Println("   - Default: \"./db.sqlite\"")
	fmt.Println("   - Note: Must be either an empty file or an sqlite database")
}
